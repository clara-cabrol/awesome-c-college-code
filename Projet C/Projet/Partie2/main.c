#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[])
{
    char expression1[10];
    char operateur;
    char expression2[10];
    char inputUser[10] = {};
    const char * ptr = inputUser; 

    double parsed_expression1;
    double parsed_expression2;
    double res = 0;    

    int status;

    while(true) {
        printf("Entrez un calcul > ");
        fgets(inputUser, 10, stdin);
        status = sscanf(ptr, "%9s %[+-*/%^] %9s", expression1, &operateur, expression2);     
        
        if (status != 3)
        {
            if (strcmp(expression1, "quit") == 0) {
                break;
            }
            else if (strcmp(expression1, "flush") == 0) {
                printf("\033c");
            }
            else if (strcmp(expression1, "res") == 0) {
                printf("%lf\n", res);
            }
            else {
                printf("undefined command: %s", inputUser);
            }
        } else {
            if (strcmp(expression1, "res") == 0) {
                parsed_expression1 = res;
            } else {
                // Conversion du string en long en base 10
                parsed_expression1 = strtol(expression1, NULL, 010);
            }
            if (strcmp(expression2, "res") == 0) {
                parsed_expression2 = res;
            } else {
                // Conversion du string en long en base 10
                parsed_expression2 = strtol(expression2, NULL, 010);
            }

            switch (operateur)
            {
            case '+':
                res = parsed_expression1 + parsed_expression2;
                break;
            case '-':
                res = parsed_expression1 - parsed_expression2;
                break;
            case '*':
                res = parsed_expression1 * parsed_expression2;
                break;
            case '/':
                res = parsed_expression1 / parsed_expression2;
                break;
            case '%':
                res = (int) parsed_expression1 % (int) parsed_expression2;
                break;
            case '^':
                res = pow(parsed_expression1, parsed_expression2);
                break;
            }
            
            printf("%s %c %s = %lf\n", expression1, operateur, expression2, res);
        }
        
    };
    return 0;
}
