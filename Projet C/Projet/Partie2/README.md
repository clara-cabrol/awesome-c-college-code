# Partie 2
## Explications
L'utilisation du `fgets` permet de lire les caractères de l'entrée stdin, et les range dans le tableau inputUser.
Ensuite, `sscanf` permet de parser l'entrée utilisateur pour déterminer s'il s'agit d'une expression à calculer, ou l'un des mots clefs avec un comportement prédéfini.

On vient alors vérifier avec status que sscanf possède bien 3 groupes. Si c'est le cas, on convertit les string en long en base 10, puis on effectue les opérations +, -, *, ... dans le switch case.

Sinon, on compare l'entrée utilisateurs avec idfférents mots clefs tels que qui, flus, res, qui déterminent un fonctionnement spécifique.