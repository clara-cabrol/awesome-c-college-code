#include"io.h"
#include<stdarg.h>
#include<stdio.h>


void printChar(char c)
{
    // Print the character to the screen
    print(1, &c, 1);
}

void printString(char* str)
{
    // Iterate through the characters in the string
    while (*str != '\0') {
        // Print the current character to the screen
        printChar(*str);

        // Advance to the next character in the string
        str++;
    }
}

void printDigit(int i)
{
    if (i >= 0 && i <= 9) {
        // Convert the digit to its ASCII representation
        char d = 0x30 + i;

        // Print the digit to the screen
        print(1, &d, 1);
    } else {
        // The input digit is not between 0 and 9, do nothing
    }
}

void printInteger(int i)
{
	int nbrInverse = 0;

	// Loop until the integer isn't fully inverted
	while (i != 0) {
		nbrInverse = nbrInverse * 10; // Multiply to 10 the inverted number to prepare for the new digit
		nbrInverse = nbrInverse + i%10; // Get the last digit of the integer and add it to the inverted number
		i = i/10; // Move out the last digit
	}

	//Loop until there is a digit to print
	while(nbrInverse != 0){
		int digit = nbrInverse%10; //Get the last digit of the inverted number
		printDigit(digit); // Print the digit 
		nbrInverse = nbrInverse/10; // Move out the last digit
	}
}

/********FUTHER 7********/
//Can't work better than this
void printFloat (float f) {
    // Handle negative numbers
    if (f < 0.0) {
        printChar('-');
        f = -f;
    }

    // Round the float to its integer value
    int i = (int)(f);

    // If the float is greater than or equal to 1, print the integer part
    if (f >= 1.0) {
        printInteger(i);
    }

    // Print the decimal point
    printChar('.');

    // Print the fractional part
    while (f >= 1.0) {
        f -= 1.0; // Subtract 1 from the floating-point number until it is less than 1
    }
    while (f > 0.0) {
        f *= 10.0; // Multiply the number by 10 to move the decimal point to the right
        int digit = (int)(f); // Extract the integer value of the number
        printInteger(digit); // Print the extracted integer value
        f -= digit; // Subtract the extracted integer value from the floating-point number
    }
}

void newPrintf(char* str, ...)
{
	// Declare a variable arguments list
	va_list args;

	// Initialize the variable arguments list, specifying the name of the last known argument
	va_start(args, str);

	// Loop through the characters in the string until a null character is found
	while (*str != '\0') {
	    // If the current character is a percent sign, then we have encountered a placeholder
	    if (*str == '%') {
		// Move to the next character in the string to determine the type of placeholder
		str++;

		// Check the type of placeholder and handle it accordingly
		if (*str == 'c') {
		    // If the placeholder is a character, expect the next argument to be a character and print it using the printChar function
		    char c = va_arg(args, int);
		    printChar(c);
		} else if (*str == 'd') {
		    // If the placeholder is an integer, expect the next argument to be an integer and print it using the printInteger function
		    int i = va_arg(args, int);
		    printInteger(i);
		} else if (*str == 's') {
		    // If the placeholder is a string, expect the next argument to be a string and print it using the printString function
		    char* string = va_arg(args, char*);
		    printString(string);
		} else if (*str == 'f') {
			// If the placeholder is a float, expect the next argument to be a float and print it using the printFloat function
			float f = va_arg(args, double);
			printFloat(f);
		} else {
		    // If the placeholder is not recognized, simply print the percent sign and the character that was found
		    printChar('%');
		    printChar(*str);
		}

		// Move to the next character in the string
		str++;
	    }

	    // If the current character is not a percent sign, simply print it and move on to the next character
	    printChar(*str);
	    str++;
	}

	// End the variable arguments list
	va_end(args);
}
